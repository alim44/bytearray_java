package ru.vchater.bytearray;

import java.nio.ByteBuffer;
import java.util.Arrays;






public class ByteArray {
	private byte[] b;
	private byte[] temp;
	public int bytesAvailable;
	public int length;
	private int position;
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	
	public ByteArray() {
		b=new byte[0];
		bytesAvailable=0;
		length=0;
		position=0;
	}
	public int position(){
		return position;
	}
	public void position(int i){
		position=i;
		bytesAvailable=length-position;
	}
	public ByteArray(byte[] bb) {
		b=bb;
		
		length=b.length;
		position=0;
		bytesAvailable=b.length;
	}
	public void writeBoolean(Boolean bool){
		temp=new byte[length+1];
		int i=0;
		for(;i<length;i++){
			temp[i]=b[i];
		}
		if(bool){
			temp[i]=(byte)1;
		}else{
			temp[i]=(byte)0;
		}
		
		b=temp;
		temp=new byte[0];
		length=b.length;
		position=b.length;
		bytesAvailable=0;
	}
	public void writeByte(byte _byte){
		temp=new byte[length+1];
		int i=0;
		for(;i<length;i++){
			temp[i]=b[i];
		}
		
		temp[i]=_byte;
		b=temp;
		temp=new byte[0];
		length=b.length;
		position=b.length;
		bytesAvailable=0;
	}
	public void writeInt(int input){
		temp=new byte[length+4];
		int i=0;
		for(;i<length;i++){
			temp[i]=b[i];
		}
		
		byte[] bytes = ByteBuffer.allocate(4).putInt(input).array();
		for(int j=0;j<bytes.length;j++){
			//I.out(String.valueOf(i+j));
			temp[i+j]=bytes[j];
		}
		
		b=temp;
		temp=new byte[0];
		length=b.length;
		position=b.length;
		bytesAvailable=0;
	}
	public void writeBytes(byte[] input){
		temp=new byte[length+input.length];
		int i=0;
		for(;i<length;i++){
			temp[i]=b[i];
		}
		for(int j=0;j<input.length;j++){
			temp[i+j]=input[j];
		}
		b=temp;
		temp=new byte[0];
		length=b.length;
		position=b.length;
		bytesAvailable=0;
	}
	public void writeBytes(byte[] input,int offset){
		if(offset<input.length){
			temp=new byte[length+input.length-offset];
			int i=0;
			for(;i<length;i++){
				temp[i]=b[i];
			}
			for(int j=offset;j<input.length;j++){
				temp[i+j-offset]=input[j];
			}
			b=temp;
			temp=new byte[0];
			length=b.length;
			position=b.length;
			bytesAvailable=0;
		}
	}
	public void writeBytes(byte[] input,int offset,int len){
		if((offset+len)<=input.length){
			temp=new byte[length+len];
			int i=0;
			for(;i<length;i++){
				temp[i]=b[i];
			}
			for(int j=offset;j<len+offset;j++){
				temp[i+j-offset]=input[j];
			}
			b=temp;
			temp=new byte[0];
			length=b.length;
			position=b.length;
			bytesAvailable=0;
		}else{
			
		}
	}
	public void writeUTFBytes(String input){
		byte[] temp_b=input.getBytes(java.nio.charset.StandardCharsets.UTF_8);
		temp=new byte[length+temp_b.length];
		int i=0;
		for(;i<length;i++){
			temp[i]=b[i];
		}
		for(int j=0;j<temp_b.length;j++){
			temp[i+j]=temp_b[j];
		}
		b=temp;
		temp=new byte[0];
		length=b.length;
		position=b.length;
		bytesAvailable=0;
	}
	public boolean readBoolean(){
		if(position<length){
			byte t=b[position];
			position++;
			bytesAvailable=length-position;
			if(t==(byte)0x00){
				return false;
			}else{
				return true;
			}
		}else{
			throw new ArrayIndexOutOfBoundsException();
		}
	}
	public int readInt(){
		if(length-position>3){
			int t=ByteBuffer.wrap(b, position, 4).getInt();
			position=position+4;
			bytesAvailable=length-position;
			return t;
		}else{
			throw new ArrayIndexOutOfBoundsException();
		}
	}
	public int readShort(){
		if(length-position>1){
			int t=ByteBuffer.wrap(b, position, 2).getShort();
			position=position+2;
			bytesAvailable=length-position;
			return t;
		}else{
			throw new ArrayIndexOutOfBoundsException();
		}
	}
	public int readByte(){
		if(length-position>0){
			int t=ByteBuffer.wrap(b, position, 1).get();
			position=position+1;
			bytesAvailable=length-position;
			return t;
		}else{
			throw new ArrayIndexOutOfBoundsException();
		}
	}
	public byte[] readBytes(int len){
		if(length-position>len-1){
			byte[] t=Arrays.copyOfRange(b, position, position+len);
			position=position+len;
			bytesAvailable=length-position;
			return t;
		}else{
			throw new ArrayIndexOutOfBoundsException();
		}
		
	}
	public String readUTFBytes(int len){
		if(length-position>len-1){
			byte[] t=Arrays.copyOfRange(b, position, position+len);
			position=position+len;
			bytesAvailable=length-position;
			return new String(t,java.nio.charset.StandardCharsets.UTF_8);
		}else{
			throw new ArrayIndexOutOfBoundsException();
		}
	}
	public int readUnsignedShort(){
		if(length-position>1){
			int t=ByteBuffer.wrap(b, position, 2).getShort();
			position=position+2;
			bytesAvailable=length-position;
			return t;
		}else{
			throw new ArrayIndexOutOfBoundsException();
		}
	}
	public String toString()
	{
		char[] hexChars = new char[b.length * 3];
	    for ( int j = 0; j < b.length; j++ ) {
	        int v = b[j] & 0xFF;
	        hexChars[j * 3] = hexArray[v >>> 4];
	        hexChars[j * 3 + 1] = hexArray[v & 0x0F];
	        hexChars[j * 3 + 2] = ':';
	    }
	    return new String(hexChars);
	}
	public byte[] toArray(){
		return b;
	}
	public void clear(){
		b=new byte[0];
		temp=new byte[0];
		position=0;
		length=0;
		bytesAvailable=0;
	}
}
